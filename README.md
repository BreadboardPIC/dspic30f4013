# dsPIC30F4013 Breakout Board

This is the first board I ever designed (at least first that was not part
of a university project, i.e. that I made for myself). Technically the
project files are a recovery since the original was designed in Eagle and
the files have long been lost. The schematic from this project was recovered
by retracing the traces from the PCBs originally produced. Perhaps one day
I will redesign the PCB...

Front Side:
![alt text](img/BoardFront.jpeg "Front view of the dsPIC board")

Back Side:
![alt text](img/BoardBack.jpeg "Back view of the dsPIC board")

## History

The original board is heavily inspired from the dsPIC-NODE board, designed
and used by IPFN. Link [here](http://metis.ipfn.tecnico.ulisboa.pt/CODAC/IPFN_Instrumentation/Boards/DsPICnode).
The dsPIC-NODE board was used as part of the Microcontrollers and
Electronic Instrumentation university courses, in particular for the lab
work. But there were not many boards available, making it difficult
or even impossible, to develop and work on the lab code outside of lab
hours.

This prompted the design of the board in this project, having the same
overall design, but removing some less frequently used components.
This allowed for a reduced cost, making it available and affordable to
the students. A large batch of boards was ordered, further contributing
to the affordability of the project and individual boards, without the
microntroller, were made available to the other students in the class.

The design concept of the board was such that it could be directly
plugged into the breadboard being used in the classes:
![alt text](img/Breadboard.jpeg "View of the dsPIC board mounted on a breadboard")


## Notes

Switches SW3 to SW10 are implemented as jumpers, allowing to control
the configuration of the board, in particular whether the microncontroller
pins 8 and 9 connect to the external edge mounted connector or to
the internal programming lines, effectively enabling and disabling
the board programming capabilities.

SW3 and SW4 form the first pair, controlling whether the UART2 pins
are connected to the MAX232 and ultimately to the DB9 serial port
or to the external connector.

SW5 and SW6 control the PGD and PGC line, as explained above.

SW7, SW8, SW9 and SW10 control whether the PICKIT header is used or not.
SW7 switches the MCLR pin between the pickit MCLR line and the RS232 TX PIN
(Technically it is possible to program the board over RS232, but this has
not been tested and the capability is there for compatibility with the
dsPIC-NODE board).
SW8 switches between the microcontroller 5V being taken from the on board
5V regulator or the pickit.
SW9 connects PGC to the PICKIT or the RS232 PGC line, if it is not 
connected to the external connector with SW6.
SW10 connects PGD to the PICKIT or the RS232 PGD line, if it is not
connected to the external connector with SW5.

All these switches with jumpers probably have consequences for the signal
integrity of the relevant lines and the design is probably not the best,
but it was good enough for the intended purpose.
For ease of use a number of boards were produced with SW7 to SW10 having
their pins shorted, effectively permenantly attaching both PICKIT and
RS232 to the relevant pins, meaning care should be taken to not connect
both simultaneously:
![alt text](img/BoardShorted.jpeg "View of the dsPIC board with some switches shorted")
